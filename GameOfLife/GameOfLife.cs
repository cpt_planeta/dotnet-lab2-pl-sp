﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Program
    {
        static void Main(string[] args)
        {
            World swiat = new World(10, 10);
            while (true)
            {
                Console.Clear();
                swiat.Print();
                Console.ReadKey();
                swiat.Live();
            }
        }
    }

    class World
    {
        enum State { Dead, Living };
        State[,] world;
        int rows;
        int columns;

        public World(int height, int width)
        {
            Random gen = new Random();
            rows = height;
            columns = width;
            world = new State[rows, columns];
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    if(gen.Next(2)==0)
                        world[i, j] = State.Dead;
                    else
                        world[i, j] = State.Living;
                }
            }
            
        }

        public void Print()
        {
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    Console.Write((int)world[i, j]);
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
        }

        private int CountLivingNeighbours(int row, int column)
        {
            int living = 0;
            int rowmin = (row - 1) >= 0 ? (row - 1) : 0;
            int rowmax = (row + 1) <= (rows - 1) ? (row + 1) : (rows - 1);
            int columnmin = (column - 1) >= 0 ? (column - 1) : 0;
            int columnmax = (column + 1) <= (columns - 1) ? (column + 1) : (columns - 1);

            for (int i = rowmin; i <= rowmax; ++i)
            {
                for (int j = columnmin; j <= columnmax; ++j)
                {
                    if (world[i, j] == State.Living && (i != row || j != column))
                        ++living;
                }
            }

            return living;
        }

        public void Live()
        {
            int living = 0;
            State[,] newworld = new State[rows, columns];
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    living = CountLivingNeighbours(i, j);
                    if ( living < 2 && ( world[i, j] == State.Living ))
                        newworld[i, j] = State.Dead;
                    else if (( living == 2 || living == 3 ) && ( world[i, j] == State.Living ))
                        newworld[i, j] = State.Living;
                    else if ( living > 3 && ( world[i, j] == State.Living ))
                        newworld[i, j] = State.Dead;
                    else if (( world[i, j] == State.Dead ) && living == 3 ) 
                        newworld[i, j] = State.Living;
                }
            }
            for (int i = 0; i < rows; ++i)
            {
                for (int j = 0; j < columns; ++j)
                {
                    world[i, j] = newworld[i, j];
                }
            }
        }
    }
}
